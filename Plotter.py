import numpy as np
import plotly.graph_objects as go
from   plotly.subplots import make_subplots
import plotly.offline as pyo
import numpy as np
from Misc.Math import *

class Plotter:
    def __init__(self,N0T,L,width_3D,SM=None):
        #CODE
        #name(_)(x,y,z)(T,L)
        self.subplot_titles = [
            "p_xT", "p_yT", "p_zT", #last t, all s
            "p_xL", "p_yL", "p_zL", #last s, all t

            "h_xT", "h_yT", "h_zT",#last t, all s
            "h_wT", "", "",

            "h_xL", "h_yL", "h_zL",#last s, all t
            "h_wL", "", "",

            "u_xT", "u_yT", "u_zT",    #last t, all s
            "mb_xT", "mb_yT", "mb_zT", #last t, all s
            "m_xT", "m_yT", "m_zT",    #last t, all s

            "v_xT", "v_yT", "v_zT",   #last t, all s
            "nb_xT", "nb_yT", "nb_zT",#last t, all s
            "n_xT", "n_yT", "n_zT",   #last t, all s

            "success","max_fun","",
            "n_itr","nfev","",""
            "time", "","",

            "p_err_L","R_err","",
            "p_err_L","R_err","not success",
            "n_itr","nfev","time",
        ]

        size = int(len(self.subplot_titles)/3)
        specs = []
        for i in range(size-2):
            specs.append([{"type": "scatter"}, {"type": "scatter"},{"type": "scatter"}])
        specs.append([{"type": "table"}, {"type": "table"},{"type": "table"}])
        specs.append([{"type": "table"}, {"type": "table"},{"type": "table"}])
        self.margin = 9

        self.fig = make_subplots(rows=size, cols=3, subplot_titles=self.subplot_titles,specs=specs)
        self.fig.update_layout(height=710*size, width = 710*3, showlegend=True, title_text="ICRA@40")

        self.SM = SM
        self.L  = L
        self.width_3D = width_3D
        self.p_err = []
        self.R_err = []
        self.time = []
        self.nfev = []
        self.n_itr = []
        self.success = []
        self.frames = [go.Frame(data=[], name=str(ti)) for ti in range(N0T)]


    def store(self,name):
        table_cells = self.sort_table(self.p_err)
        gotable = goTable(table_cells)
        add_trace(self.fig,gotable,17,1,"","")

        table_cells = self.sort_table(self.R_err)
        gotable = goTable(table_cells)
        add_trace(self.fig,gotable,17,2,"","")

        table_cells = self.sort_table(self.success)
        gotable = goTable(table_cells, ["Method","Sum"])
        add_trace(self.fig,gotable,17,3,"","")

        table_cells = self.sort_table(self.n_itr)
        gotable = goTable(table_cells)
        add_trace(self.fig,gotable,18,1,"","")

        table_cells = self.sort_table(self.nfev)
        gotable = goTable(table_cells)
        add_trace(self.fig,gotable,18,2,"","")
        
        table_cells = self.sort_table(self.time)
        gotable = goTable(table_cells)
        add_trace(self.fig,gotable,18,3,"","")

        path_name = "Storage//"+name
        pyo.plot(self.fig, filename=path_name+".html", auto_open=False)

        filename_3D = "plot_3D"
        self.store_3D(self.frames,self.L*2,filename_3D)
        iframe_embed_code = f'<iframe src="{filename_3D+".html"}" width="100%" height="710"></iframe>'
        with open('Storage//'+name+'.html', 'a') as f:
            f.write(iframe_embed_code)

    def plot2D(self,method):
        s_eval = np.linspace(0,method.MP.L,method.N0L)
        t_eval = np.linspace(0,method.N0T,method.N0T)
        color = method.color
        dash  = method.dash
        name  = method.name
        showlegend = True
        for i in range(len(self.subplot_titles)-self.margin):
            row = int(i/3)+1
            col = int(i%3)+1
            sub_title = self.subplot_titles[i]
            if not sub_title == "":
                char_1 = sub_title[-1]
                char_2 = sub_title[-2]
                if char_1 == "T":
                    mat = method.history.state[sub_title[0:-3]]
                    if char_2 == "x":
                        vec = mat[-1,:,0]
                    elif char_2 == "y":
                        vec = mat[-1,:,1]
                    elif char_2 == "z":
                        vec = mat[-1,:,2]
                    elif char_2 == "w":
                        vec = mat[-1,:,3]
                    scatter = goScatter(s_eval,vec,"lines",name,color,dash,showlegend)
                    add_trace(self.fig,scatter,row,col,"s",char_2)
                elif char_1 == "L":
                    vec = method.history.state[sub_title[0:-3]]
                    if char_2 == "x":
                        vec = mat[:,-1,0]
                    elif char_2 == "y":
                        vec = mat[:,-1,1]
                    elif char_2 == "z":
                        vec = mat[:,-1,2]
                    elif char_2 == "w":
                        vec = mat[:,-1,3]
                    scatter = goScatter(t_eval,vec,"lines",name,color,dash,showlegend)
                    add_trace(self.fig,scatter,row,col,"loading_steps",char_2)
                else:
                    vec = method.history.state[sub_title]
                    scatter = goScatter(t_eval,vec,"lines",name,color,dash,showlegend)
                    add_trace(self.fig,scatter,row,col,"loading_steps",sub_title)
                showlegend = False

        vec = method.history.state["success"]
        self.success.append([name,method.N0T-np.sum(vec)])
        vec = method.history.state["n_itr"]
        self.n_itr.append([name,np.mean(vec),np.max(vec)])
        vec = method.history.state["nfev"]
        self.nfev.append([name,np.mean(vec),np.max(vec)])
        vec = method.history.state["time"]
        self.time.append([name,np.mean(vec),np.max(vec)])

        if self.SM is not None and self.SM != method:
            L = self.SM.MP.L
            i = len(self.subplot_titles)-self.margin
            row = int(i/3)+1
            col = int(i%3)+1
            sub_title = self.subplot_titles[i]
            vec = (np.linalg.norm(method.history.state["p"][:,-1] - self.SM.history.state["p"][:,-1],axis=1)/L)*100

            self.p_err.append([name,np.mean(vec),np.max(vec)])
            scatter = goScatter(t_eval,vec,"lines",name,color,dash,showlegend)
            add_trace(self.fig,scatter,row,col,"loading_steps",sub_title)

            #-------------------------------------------
            i = len(self.subplot_titles)-self.margin+1
            row = int(i/3)+1
            col = int(i%3)+1
            sub_title = self.subplot_titles[i]
         
            R_SM = self.SM.history.state["R"][:,-1]
            R_RM = method.history.state["R"][:,-1]

            R_SM_R_RM_T = np.einsum('nij,njk->nik', R_SM, np.transpose(R_RM, (0, 2, 1)))
            vec = np.arccos((np.einsum('nii->n', R_SM_R_RM_T) - 1) / 2)  
            self.R_err.append([name,np.mean(vec),np.max(vec)])

            scatter = goScatter(t_eval,vec,"lines",name,color,dash,showlegend)
            add_trace(self.fig,scatter,row,col,"loading_steps",sub_title)

        #---------------------------------------
        for ti in range(method.N0T):
            pi  = method.history.state["p"][ti]
            self.frames[ti].data += (go_scatter3d(pi, color, self.width_3D, name, dash),)


    def store_3D(self, frames, range, filename_3D):
        fig3D = go.Figure(data=frames[0].data)
        fig3D.frames = frames
        fig3D.update_layout(
            title='ICRA@40', 
            margin=dict(l=0, r=0, b=0, t=0),
            scene=dict(
                xaxis=dict(title='x [m]', range=[-range, range], showgrid=True),
                yaxis=dict(title='y [m]', range=[-range, range], showgrid=True),
                zaxis=dict(title='z [m]', range=[-range, range], showgrid=True),
                
                aspectmode='manual',
                aspectratio=dict(x=1, y=1, z=1),
            ),
            legend=dict(
                x=1.05,  
                y=0.5,   
                xanchor='left',  
                yanchor='bottom'  
            ),
            sliders=[{
                'pad': {"t": 0},
                'steps': [{'args': [[f.name], {'frame': {'duration': 500, 'redraw': True}, 'mode': 'immediate', 'fromcurrent': True}],
                        'label': f.name, 'method': 'animate'} for f in fig3D.frames]
            }],
            updatemenus=[{
                'type': 'buttons',
                'showactive': True,
                'y': 0,
                'x': 0,
                'xanchor': 'right',
                'yanchor': 'top',
                'pad': {'t': 0, 'r': 0},
                'buttons': [
                    {'args': [None, {'frame': {'duration': 500, 'redraw': True}, 'fromcurrent': True, 'transition': {'duration': 300, 'easing': 'linear'}}],
                    'label': 'Play', 'method': 'animate'},
                    {'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate', 'transition': {'duration': 0}}],
                    'label': 'Pause', 'method': 'animate'}
                ]
            }],
        )

        path = "Storage//"+filename_3D+".html"
        fig3D.write_html(path)
        print("3D animation is stored in",path)
        return fig3D

    def sort_table(self,arr,wrt=1,ndigits=3):
        vectorized_round = np.vectorize(round_to_significant_digits)
        sorted_arr = np.array(sorted(arr, key=lambda x: float(x[wrt]))).T
        sorted_names = sorted_arr[0,:]
        safe_arr = safe_convert(sorted_arr)
        table_cells = vectorized_round(safe_arr,ndigits)
        table_cells[0,:] = sorted_names
        return table_cells

def go_scatter3d(v_s, color, width, name, dash):
        scatter3d = go.Scatter3d(x=v_s[:, 0],
                     y=v_s[:, 1],
                     z=v_s[:, 2],
                     mode='lines',
                     name = name,
                     line=dict(color=color, width=width, dash=dash),
                     showlegend=True
                    )
        return scatter3d

def goTable(cells,header = ['Method', 'Average', 'Max.']):
        res = go.Table(header =dict(values=header),
                           cells  =dict(values=cells))
        return res

def goScatter(x,y,mode,name,color,dash,showlegend):
    res = go.Scatter(x = x,
                     y = y,
                     mode=mode,
                     name=name,
                     legendgroup = name,
                     line=dict(color=color,dash=dash),
                     showlegend = showlegend
                     )
    return res

def add_trace(fig,scatter,row,col,x_label,y_label):
    fig.add_trace   (scatter           ,row=row,col=col)
    fig.update_xaxes(title_text=x_label,row=row,col=col)
    fig.update_yaxes(title_text=y_label,row=row,col=col)
