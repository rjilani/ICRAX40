
import copy

class Simulation:
    def __init__(self, methods, arr_BCL, IF):
        self.methods = copy.deepcopy(methods)
        self.arr_BCL = arr_BCL
        self.IF = IF 

    def run(self,ti):
        for method in self.methods:
            method.run(ti,self.arr_BCL[ti],self.IF)

