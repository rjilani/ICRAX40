import numpy as np
from Misc.Math import *
from scipy.integrate import solve_ivp
from Misc.History import *
import time

colors = np.array(['#1f77b4', '#ff7f0e', '#2ca02c',
                   '#d62728', '#9467bd', '#8c564b',
                   '#e377c2', '#7f7f7f', '#bcbd22',
                   '#17becf'])

class ASP:
    """
    phiq = u-usr
    """
    def __init__(self,MP,N0T,N0L,max_step,qk):
        self.history = History(N0T,N0L)
        self.MP  = MP
        self.N0T = N0T
        self.N0L = N0L
        self.max_step = max_step
        self.qk = qk
        self.qn = sum(qk)
        self.s_eval = np.linspace(0,MP.L,N0L)
        self.q_ig = np.zeros(self.qn) 
        self.nb0  = np.zeros(3)
        self.color = colors[qk[0]-1]
        self.dash  = "dash"
        self.name  = "ASP_"+str(self.qk[0])

    
    def run(self,ti,BCL,IF):
        t0 = time.time()
        sol = newton_raphson(self.residual,self.q_ig,args=(BCL,IF))
        t1 = time.time()
        mytime = t1-t0
        self.q_ig = sol.x
        print("R1",sol.success,sol.n_itr,sol.nfev,"{:.0e}".format(max(abs(sol.fun))),"{:.3f}".format(mytime))
        self.reconstruction(ti,sol,mytime)

    def residual(self,q,BCL,IF):
        L  = self.MP.L
        qn = self.qn

        ya0 = np.eye(3).flatten()
        Ya = solve_ivp(self.R_ODE,(0,L),y0=ya0,args=([q]),max_step=self.max_step).y.T
        RL = Ya[-1].reshape((3,3))

        if IF:
            BCL = np.concatenate([RL.T@BCL[0:3],RL.T@BCL[3:6]])

        #Y=(R(0:9),nb(9:12),mb(12:15),y(15:15+qn))
        yb0 = np.concatenate([RL.flatten(),BCL,np.zeros(qn)])
        Yb = solve_ivp(self.ODE,(L,0),y0=yb0,args=([q]),max_step=self.max_step).y.T
        self.nb0[:] = Yb[-1,9:12]
        
        resi = Yb[-1,15:]
        return resi

    def reconstruction(self,ti,sol,mytime):
        L = self.MP.L
        N0L = self.N0L
        Kbt = self.MP.Kbt
        invKse = self.MP.invKse
        usr = self.MP.usr
        vsr = self.MP.vsr
        q = sol.x
        qk = self.qk
        qn = self.qn
        
        #Y(p(0:3),R(3:12),nb(12:15))
        p0 = np.zeros(3)
        R0 = np.eye(3)
        y0 = np.concatenate([p0,R0.flatten(),self.nb0,np.zeros(1)])
        Y = solve_ivp(self.pRnblng_ODE,(0,L),y0=y0,t_eval=self.s_eval,args=([q]),max_step=self.max_step).y.T
        
        p   = Y[:,0:3  ]
        R   = Y[:,3:12 ].reshape((-1,3,3))
        nb  = Y[:,12:15]
        lng = Y[-1,15  ]

        h = np.zeros((N0L,4))
        v = np.zeros((N0L,3))
        n = np.zeros((N0L,3))
        u  = np.zeros((N0L,3))
        mb = np.zeros((N0L,3))
        m  = np.zeros((N0L,3))

        for i in range(N0L):
            h[i] = to_quat(R[i])

            Phi = Phi_basis(self.s_eval[i],L,qn,qk)
            u[i] = Phi@q + usr
            mb[i] = Kbt@(u[i]-usr)
            m[i]  = R[i]@mb[i]

            v[i]  = invKse@nb[i] + vsr
            n[i]  = R[i]@nb[i]

        lst = {
            "p": p,
            "R": R,
            "h": h,

            "v" : v,
            "nb": nb,
            "n" : n,

            "u" : u,
            "mb": mb,
            "m" : m,

            "lng": lng,
            "success": sol.success,
            "max_fun": max(abs(sol.fun)),
            "nfev": sol.nfev,
            "n_itr": sol.n_itr,
            "time": mytime
        }

        self.history.update(ti,lst)

  
    def R_ODE(self,s,Y,q):
        """
        Y = R(0:9)
        """ 
        L   = self.MP.L
        usr = self.MP.usr
        qk  = self.qk
        qn  = self.qn

        R   = Y.reshape((3,3))
        
        Phi = Phi_basis(s,L,qn,qk)
        u = Phi@q + usr
        
        Rs = R@hat(u)
        res = Rs.flatten()

        return res 

    def ODE(self,s,Y,q):
        """
        Y=(R(0:9),nb(9:12),mb(12:15),y(15:15+qn))
        """
        L      = self.MP.L
        Kbt    = self.MP.Kbt
        invKbt = self.MP.invKbt
        invKse = self.MP.invKse
        f      = self.MP.rhoAg
        usr    = self.MP.usr
        vsr    = self.MP.vsr
        qn     = self.qn
        qk     = self.qk

        Phi = Phi_basis(s,L,qn,qk)
        hu = hat(Phi@q + usr)

        R  = Y[0:9].reshape((3,3))
        nb = Y[9:12]
        mb = Y[12:15]
        hv = hat(invKse@nb + vsr)

        Rs  =  R@hu
        nbs = -hu@nb-R.T@f
        mbs = -hu@mb-hv@nb #-R.T@l

        ys  = Phi.T@(Kbt@Phi@q-mb)

        res = np.concatenate([Rs.flatten(),nbs,mbs,ys])
        return res

    def pRnblng_ODE(self,s,Y,q):
        """
        Y(p(0:3),R(3:12),nb(12:15),lng(15))
        """
        L      = self.MP.L
        invKse = self.MP.invKse
        f      = self.MP.rhoAg
        usr    = self.MP.usr
        vsr    = self.MP.vsr
        qn     = self.qn
        qk     = self.qk

        R   = Y[3:12].reshape((3,3))
        nb  = Y[12:15]
        Phi = Phi_basis(s,L,qn,qk)

        u  = Phi@q     + usr
        v  = invKse@nb + vsr
        hu = hat(u)

        ps  = R@v
        Rs  = R@hu
        nbs = -hu@nb-R.T@f
        lngs = np.linalg.norm(ps)

        res = np.concatenate([ps,Rs.flatten(),nbs,[lngs]])
        return res

