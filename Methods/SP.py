import numpy as np
from Misc.Math import *
from scipy.integrate import solve_ivp
from Misc.History import *
import time

colors = np.array(['#1f77b4', '#ff7f0e', '#2ca02c',
                   '#d62728', '#9467bd', '#8c564b',
                   '#e377c2', '#7f7f7f', '#bcbd22',
                   '#17becf'])

class SP:
    """
    phiq[0:3] = u-usr
    phiq[3:6] = v-vsr
    """
    def __init__(self,MP,N0T,N0L,max_step,qk):
        self.history = History(N0T,N0L)
        self.MP  = MP
        self.N0T = N0T
        self.N0L = N0L
        self.max_step = max_step
        self.qk = qk
        self.qn = sum(qk)
        self.s_eval = np.linspace(0,MP.L,N0L)
        self.q_ig = np.zeros(self.qn) 
        self.color = colors[qk[0]-1]
        self.dash = "dot"
        self.name  = "SP_"+str(qk[0])

    def run(self,ti,BCL,IF):
        t0 = time.time()
        sol = newton_raphson(self.residual,self.q_ig,args=(BCL,IF))
        t1 = time.time()
        mytime = t1-t0
        self.q_ig = sol.x
        print("R0",sol.success,sol.n_itr,sol.nfev,"{:.0e}".format(max(abs(sol.fun))),"{:.3f}".format(mytime))
        self.reconstruction(ti,sol,mytime)

    def residual(self,q,BCL,IF):
        L = self.MP.L
        qn = self.qn

        R0  = np.eye(3)
        ya0 = R0.flatten()
        Ya  = solve_ivp(self.R_ODE,(0,L),y0=ya0,args=([q]),max_step=self.max_step).y.T
        RL  = Ya[-1].reshape((3,3))

        if IF:
            nbLmbL = np.concatenate([RL.T@BCL[0:3],RL.T@BCL[3:6]])
        else:
            nbLmbL = BCL

        #Y=(R(0:9),nb(9:12),mb(12:15),y0y1(15:15+qn))
        yb0 = np.concatenate([RL.flatten(),nbLmbL,np.zeros(qn)])
        Yb = solve_ivp(self.ODE,(L,0),y0=yb0,args=([q]),max_step=self.max_step).y.T
        resi = Yb[-1,15:]
        
        return resi
    
    def ODE(self,s,Y,q):
        #Y=(R(0:9),nb(9:12),mb(12:15),y0y1(15:15+qn))
        L = self.MP.L
        f  = self.MP.rhoAg
        Kbt = self.MP.Kbt
        Kse = self.MP.Kse
        # invKbt = self.MP.invKbt
        # invKse = self.MP.invKse
        usr    = self.MP.usr
        vsr    = self.MP.vsr
        
        qn = self.qn
        qk = self.qk

        Phi = Phi_basis(s,L,qn,qk)
        bnd = sum(qk[0:3])
        Phi0 = Phi[0:3,0:bnd]
        Phi1 = Phi[3:6,bnd:]
        Phiq = Phi@q
        Phiq0 = Phiq[0:3]
        Phiq1 = Phiq[3:6]

        hu = hat(Phiq0 + usr)
        hv = hat(Phiq1 + vsr)

        R  = Y[0:9].reshape((3,3))
        nb = Y[9:12]
        mb = Y[12:15]
      
        Rs  =  R@hu
        nbs = -hu@nb-R.T@f
        mbs = -hu@mb-hv@nb #-R.T@l

        y0s  = Phi0.T@(-mb+Kbt@Phiq0)
        y1s  = Phi1.T@(-nb+Kse@Phiq1)

        res = np.concatenate([Rs.flatten(),nbs,mbs,y0s,y1s])
        return res
    
    def R_ODE(self,s,Y,q):
        #Y = R(0:9) 
        L   = self.MP.L
        qn  = self.qn
        qk  = self.qk
        Xio = np.concatenate([self.MP.usr,self.MP.vsr])

        R   = Y.reshape((3,3))
        Phi = Phi_basis(s,L,qn,qk)
        Xi  = Phi@q + Xio
        u   = Xi[0:3]
        Rs  = R@hat(u)

        res = Rs.flatten()
        return res 
    
    def pRlng_ODE(self,s,Y,q):
        #Y = p(0:3,R(3:12),lng(12)) 
        L   = self.MP.L
        qn  = self.qn
        qk  = self.qk
        Xio = np.concatenate([self.MP.usr,self.MP.vsr])

        R   = Y[3:12].reshape((3,3))
        Phi = Phi_basis(s,L,qn,qk)
        Xi  = Phi@q + Xio

        u  = Xi[0:3]
        v  = Xi[3:6]
        hu = hat(u)

        ps = R@v
        Rs = R@hu
        lng = np.linalg.norm(R@v)
        
        res = np.concatenate([ps,Rs.flatten(),[lng]])
        return res 

    def reconstruction(self,ti,sol,mytime):
        L = self.MP.L
        s_eval = self.s_eval
        q = sol.x
        N0L = self.N0L
        MP = self.MP
        qk = self.qk
        qn = self.qn
        Kse = MP.Kse
        Kbt = MP.Kbt
        
        p0 = np.zeros(3)
        R0 = np.eye(3)

        #Y = p(0:3,R(3:12),lng(12)) 
        y0 = np.concatenate([p0,R0.flatten(),[0]])
        Ya = solve_ivp(self.pRlng_ODE,(0,L),y0=y0,t_eval=s_eval,args=([q]),max_step=self.max_step).y.T
        p = Ya[:,0:3]
        R = Ya[:,3:12].reshape((-1,3,3))
        lng = Ya[-1,12]

        h = np.zeros((N0L,4))
        u = np.zeros((N0L,3))
        mb = np.zeros((N0L,3))
        m = np.zeros((N0L,3))
        v = np.zeros((N0L,3))
        nb = np.zeros((N0L,3))
        n = np.zeros((N0L,3))

        usr = MP.usr
        vsr = MP.vsr
        Xio = np.concatenate([usr,vsr])
        for i in range(N0L):
            h[i] = to_quat(R[i])

            Phi = Phi_basis(s_eval[i],L,qn,qk)
            uvi = Phi@q + Xio

            u[i] = uvi[0:3]
            mb[i] = Kbt@(u[i]-usr)
            m[i]  = R[i]@mb[i]

            v[i]  = uvi[3:6]
            nb[i] = Kse@(v[i]-vsr)
            n[i]  = R[i]@nb[i]


        lst = {
            "p": p,
            "R": R,
            "h": h,

            "v": v,
            "nb": nb,
            "n": n,

            "u": u,
            "mb": mb,
            "m": m,

            "lng": lng,
            "success": sol.success,
            "max_fun": max(abs(sol.fun)),
            "nfev": sol.nfev,
            "n_itr": sol.n_itr,
            "time": mytime
        }

        self.history.update(ti,lst)


        