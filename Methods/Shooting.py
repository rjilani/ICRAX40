import numpy as np
from Misc.Math import *
from scipy.integrate import solve_ivp
from scipy.optimize import root
import time
from Misc.History import *

class Shooting:
    def __init__(self,MP,N0T,N0L,max_step):
        self.history = History(N0T,N0L)
        self.MP      = MP
        self.N0T = N0T
        self.N0L = N0L
        self.max_step = max_step
        self.nbmb0_ig = np.zeros(6) 
        self.s_eval = np.linspace(0,MP.L,N0L)
        
        self.color = "black"
        self.dash  = "solid"
        self.name  = "SM"

    def run(self,ti,BCL,IF):
        t0 = time.time()
        sol = root(self.residual,self.nbmb0_ig,args=(BCL,IF))
        t1 = time.time()
        mytime = t1-t0
        self.nbmb0_ig = sol.x
        print("SM",sol.success,sol.nfev,"{:.0e}".format(max(abs(sol.fun))),"{:.3f}".format(mytime))
        self.reconstruction(ti,sol,mytime)

    def residual(self,nb0mb0,BCL,IF):
        """Y=[p(0:3),R(3:12),nb(12:15),mb(15:18),lng(18)]"""
        L = self.MP.L
        p0 = np.zeros(3)
        R0 = np.eye(3)
        y0 = np.concatenate([p0,R0.flatten(),nb0mb0,[0]])
        Y  = solve_ivp(self.ODE,(0,L),y0,max_step=self.max_step).y.T

        nbLmbL = Y[-1,12:18]
        if IF:
            RL   = Y[-1, 3:12].reshape((3,3))
            nLmL = np.concatenate([RL@nbLmbL[0:3],RL@nbLmbL[3:6]])
            res  = nLmL - BCL 
        else:
            res  = nbLmbL - BCL
        
        return res
    
    def ODE(self,s,Y):
        """Y=[p(0:3),R(3:12),nb(12:15),mb(15:18),lng(18)]"""
        del s
        invKbt = self.MP.invKbt
        invKse = self.MP.invKse
        usr    = self.MP.usr
        vsr    = self.MP.vsr
        f      = self.MP.rhoAg

        R  = Y[3:12].reshape((3,3))
        nb = Y[12:15]
        mb = Y[15:18]
        
        v = invKse@nb + vsr
        u = invKbt@mb + usr

        hu = hat(u)
        hv = hat(v)

        ps   = R@v
        Rs   = R@hu
        nbs  = -hu@nb - R.T@f
        mbs  = -hu@mb-hv@nb #-R.T@l
        lngs = np.linalg.norm(R@v)
        # lngs = 0

        res = np.concatenate([ps,Rs.flatten(),nbs,mbs,[lngs]])
        return res

    def reconstruction(self,ti,sol,mytime):
        """Y=[p(0:3),R(3:12),nb(12:15),mb(15:18),lng(18)]"""
        p0 = np.zeros(3)
        R0 = np.eye(3)
        y0 = np.concatenate([p0,R0.flatten(),sol.x,[0]])
        Y = solve_ivp(self.ODE,(0,self.MP.L),y0,t_eval=self.s_eval,max_step=self.max_step).y.T
        
        p   = Y[:,0:3]
        R   = Y[:,3:12].reshape((-1,3,3))
        nb  = Y[:,12:15]
        mb  = Y[:,15:18]
        lng = Y[-1,18]
        #----------

        invKse = self.MP.invKse
        invKbt = self.MP.invKbt
        vsr    = self.MP.vsr
        usr    = self.MP.usr

        v = np.zeros((self.N0L,3))
        u = np.zeros((self.N0L,3))
        m = np.zeros((self.N0L,3))
        n = np.zeros((self.N0L,3))
        h = np.zeros((self.N0L,4))

        for i in range(self.N0L):
            v[i] = invKse@nb[i]+vsr
            u[i] = invKbt@mb[i]+usr
            m[i] = R[i]@mb[i]
            n[i] = R[i]@nb[i]
            h[i] = to_quat(R[i])

        lst = {
            "p": p,
            "R": R,
            "h": h,

            "v": v,
            "nb": nb,
            "n": n,

            "u": u,
            "mb": mb,
            "m": m,

            "lng": lng,
            "success": sol.success,
            "max_fun": max(abs(sol.fun)),
            "nfev": sol.nfev,
            "n_itr": -1, #Newton-Raphson is not used for the shooting method
            "time": mytime
        }
        self.history.update(ti,lst)

