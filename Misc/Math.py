import numpy as np
from scipy.spatial.transform import Rotation
import uuid
import pickle 
import math

def hat(y):
    return np.array([[0, -y[2], y[1]],
                    [y[2], 0, -y[0]],
                    [-y[1], y[0], 0]])

def to_quat(R):
    rotation = Rotation.from_matrix(R)
    h = rotation.as_quat()
    return h

#https://github.com/TIMClab-CAMI/Cosserat-Rod-Modeling-of-Tendon-Actuated-Continuum-Robots
def Phi_basis(X, l, n, dim_k):
    X = X / l 
    M_cheb = np.array([
        [1, 0, 0, 0, 0, 0, 0, 0],
        [-1, 2, 0, 0, 0, 0, 0, 0],
        [1, -6, 6, 0, 0, 0, 0, 0],
        [-1, 12, -30, 20, 0, 0, 0, 0],
        [1, -20, 90, -140, 70, 0, 0, 0],
        [-1, 30, -210, 560, -630, 252, 0, 0],
        [1, -42, 420, -1680, 3150, -2772, 924, 0],
        [-1, 56, -756, 4200, -11550, 16632, -12012, 3432],
        [1, -72, 1260, -9240, 34650, -72072, 84084, -51480],
        [-1, 90, -1980, 18480, -90090, 252252, -420420, 411840],
        [1, -110, 2970, -34320, 210210, -756756, 1681680, -2333760]
    ])

    X_powers = np.array([1, X, X**2, X**3, X**4, X**5, X**6, X**7])
    b = M_cheb @ X_powers

    nb = dim_k.shape[0]
    Phi = np.zeros((nb, n))
    idx = 0
    for i in range(nb):  # for each of the xyz linear and angular strains
        k = dim_k[i]
        Phi[i, idx:idx + k] = b[:k]
        idx += k
    return Phi

class SOL:
    def __init__(self, x, fx, n_itr, nfev, J, success):
        self.x = x
        self.fun = fx
        self.n_itr = n_itr
        self.nfev = nfev
        self.jac = J
        self.success = success
    def __str__(self):
            return (f"x={self.x},\n\n fun={self.fun},\n\n nfev={self.nfev},\n\n "
                    f"jac={self.jac},\n\n success={self.success}")
    
def newton_raphson(f, x0, tol=1e-7, max_iter=7, args=(), kwargs={}):
    def approx_jacobian(x, f, f0, h=1e-8, args=args, kwargs=kwargs):
        n = len(x)
        jac = np.zeros((n, n))
        for i in range(n):
            x1 = np.array(x)
            x1[i] += h
            fx1 = f(x1, *args, **kwargs)
            jac[:, i] = (fx1 - f0) / h
        return jac
    x = x0
    for i in range(max_iter):
        fx = f(x, *args, **kwargs)
        J = approx_jacobian(x, f, fx, args=args, kwargs=kwargs)
        if np.linalg.norm(fx) < tol:
            sol = SOL(x,fx,i+1,(i+1)*(1+fx.shape[0]),J,True)
            return sol
        delta = np.linalg.solve(J, fx)
        x -= delta

    sol = SOL(x,fx,i+1,(i+1)*(1+fx.shape[0]),J,False)
    return sol


def round_to_significant_digits(x, n):
    if x == 0:
        return 0
    if math.isnan(x):
        pass
    else:
        scale = np.floor(np.log10(np.abs(x)))
        return round(x, int(n - 1 - scale))
    
def safe_convert(arr):
    numeric_arr = np.empty(arr.shape, dtype=np.float64)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            try:
                numeric_arr[i, j] = float(arr[i, j])
            except ValueError:
                numeric_arr[i, j] = np.nan
    return numeric_arr
    
def store_sim(sim,app_name,k=None):
    unique_name = str(uuid.uuid4())
    pkl_path = "Storage//"+app_name+"//"+unique_name+".pkl"
    with open(pkl_path, 'wb') as file:
        pickle.dump(sim, file)

    if k is not None:
        record = pkl_path+" k "+str(k)+" N0L "+str(sim.methods[0].N0L)+" N0T "+str(sim.methods[0].N0T)+" "+str(sim.methods)+"\n"
    else:
        record = pkl_path+" N0L "+str(sim.methods[0].N0L)+" N0T "+str(sim.methods[0].N0T)+" "+str(sim.methods)+"\n"

    record_path = "Storage//record.txt"
    with open(record_path, 'a') as file:
        file.write(record)

    print("Simulation stored in", pkl_path)
