import numpy as np
import math

class MechanicalProperties:
    def __init__(self, L, E, r, rho, nu, vsr, usr, g, qk = None, qn = None, empty_obj = False):
        if not empty_obj:
            A = math.pi*r**2      # Cross-sectional area
            I = math.pi*r**4/4    # Area moment of inertia
            J = 2 * I             # Polar moment of inertia
            G = E/(2*(1+nu));     # Shear modulus
            
            GA = G*A
            EA = E*A
            EI = E*I
            GJ = G*J

            # if follower tip force app : use these params instead 
            # EA = 4.2e8
            # EI = 3.5e7
            # GJ = 3.5e7
            # GA = 1.61e8

            Kse = np.diag([GA, GA, EA])   # Stiffness matrix for shear and extension for vsr = [0, 0, 1]
            Kbt = np.diag([EI, EI, GJ])   # Stiffness matrix for bending and torsion 
            invKse = np.linalg.inv(Kse)                
            invKbt = np.linalg.inv(Kbt)
            rhoAg = rho*A*g  

            #-----------------------------------------------------------
            self.L = L      #initial length 
            self.g = g      #Gravity
            self.rho = rho  #Density
            self.E = E      #Young's modulus
            self.r = r      #Radius
            self.nu = nu    #Poisson's ratio
            self.vsr = vsr  #Translational rate of change at rest
            self.usr = usr  #Rotational rate of change at rest
            self.qk  = qk
            self.qn  = qn
            #-----------------------------------------------------------
            self.A = A
            self.I = I
            self.J = J
            self.G = G
            self.GA = GA
            self.EA = EA
            self.EI = EI
            self.GJ = GJ
            self.Kse = Kse
            self.Kbt = Kbt
            self.invKse = invKse
            self.invKbt = invKbt
            self.rhoAg  = rhoAg


