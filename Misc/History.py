import numpy as np
import copy

class History:
    def __init__(self,N0T,N0L):
        self.N0L = N0L
        self.N0T = N0T

        self.p = np.zeros((N0T,N0L,3))
        self.R = np.zeros((N0T,N0L,3,3))
        self.h = np.zeros((N0T,N0L,4))
        
        self.v = np.zeros((N0T,N0L,3))
        self.nb = np.zeros((N0T,N0L,3))
        self.n = np.zeros((N0T,N0L,3))

        self.u = np.zeros((N0T,N0L,3))
        self.mb = np.zeros((N0T,N0L,3))
        self.m = np.zeros((N0T,N0L,3))

        self.lng = np.zeros(N0T)

        self.success = np.zeros(N0T)
        self.max_fun = np.zeros(N0T)
        self.nfev = np.zeros(N0T)
        self.n_itr = np.zeros(N0T)
        
        self.time = np.zeros(N0T)
        # self.state = None
        self.state = {
            "p":self.p,
            "R":self.R,
            "h":self.h,

            "v":self.v,
            "nb":self.nb,
            "n":self.n,

            "u":self.u,
            "mb":self.mb,
            "m":self.m,

            "lng":self.lng,

            "success":self.success,
            "max_fun":self.max_fun,
            "nfev":self.nfev,
            "n_itr":self.n_itr,

            "time":self.time,
        }


    def update(self,ti,obj):
        self.state["p"][ti] = copy.deepcopy(obj["p"])
        self.state["R"][ti] = copy.deepcopy(obj["R"])
        self.state["h"][ti] = copy.deepcopy(obj["h"])

        self.state["v"][ti] = copy.deepcopy(obj["v"])
        self.state["nb"][ti] = copy.deepcopy(obj["nb"])
        self.state["n"][ti] = copy.deepcopy(obj["n"])

        self.state["u"][ti] = copy.deepcopy(obj["u"])
        self.state["mb"][ti] = copy.deepcopy(obj["mb"])
        self.state["m"][ti] = copy.deepcopy(obj["m"])

        self.state["lng"][ti] = copy.deepcopy(obj["lng"])

        self.state["success"][ti] = copy.deepcopy(obj["success"])
        self.state["max_fun"][ti] = copy.deepcopy(obj["max_fun"])
        self.state["nfev"][ti] = copy.deepcopy(obj["nfev"])
        self.state["n_itr"][ti] = copy.deepcopy(obj["n_itr"])

        self.state["time"][ti] = copy.deepcopy(obj["time"])

        

