from Misc.MechanicalProperties import * 
from Methods.Shooting import *
from Methods.SP import *
from Methods.ASP import *
from Methods.Simulation import * 
import numpy as np
from Plotter import *
from Misc.Math import *

g = np.zeros(3)
L = 100
E = 4.015e8 
r = 0.57
rho = 0
nu = 0

vsr = np.array([0,0,1])
usr = np.zeros(3)

MP = MechanicalProperties(L,E,r,rho,nu,vsr,usr,g)

N0L = 100
max_step = L/N0L
N0T = 21

NL = np.array([130000,0,0])
ML = np.zeros(3)
BCLmax = np.concatenate([NL,ML])
arr_BCL = np.linspace(np.zeros(6),BCLmax,N0T)
IF = False

# k = 8
k = 4
# k = 2

SM = Shooting(MP,N0T,N0L,max_step)

qk0 = np.array([k]*6)
SP_c = SP(MP,N0T,N0L,max_step,qk0)

qk1 = np.array([k]*3)
ASP_c = ASP(MP,N0T,N0L,max_step,qk1)

# methods = [SP_c,ASP_c]
methods = [SM]

sim = Simulation(methods,arr_BCL,IF)

for i in range(arr_BCL.shape[0]):
    print("------------------------------")
    print("i",i+1,"of",N0T)
    sim.run(i)

store_sim(sim,"follower_tip_force",k)

