<i>Submitted to ICRA@40</i>
<h3><b> Angular Strain Parameterization for Solving Static Cosserat Rods</b></h3>

- Radhouane Jilani*, Pierre-Frédéric Villard*, Erwan Kerrien*
- *Universite de Lorraine, CNRS, Inria, LORIA, F-54000 Nancy, France

This repository is dedicated to the implementation of the aforementioned paper. It includes two primary scripts for executing the paper's numerical applications.

