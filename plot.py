import pickle
from Plotter import *


#45_bending paths
pkl_paths=[
"Storage//45_bending//0dcf0b4f-ef60-4dc4-b825-3b7687d3d8d9.pkl",# k 2 N0L 100 N0T 8 [<Methods.Shooting.Shooting object at 0x000002BAD124D370>]
"Storage//45_bending//c2612fd3-96c3-4a81-a862-838eb0456541.pkl",# k 2 N0L 100 N0T 8 [<Methods.SP.SP object at 0x0000011B2FA0A280>, <Methods.ASP.ASP object at 0x0000011B2FA0A370>]
"Storage//45_bending//3856af14-a12f-4cc4-841e-7b5d0df66d08.pkl",# k 4 N0L 100 N0T 8 [<Methods.SP.SP object at 0x000002196365C1F0>, <Methods.ASP.ASP object at 0x000002196365C2E0>]
"Storage//45_bending//34ca517e-55b0-4378-b26c-e2b4e65e0b67.pkl",# k 8 N0L 100 N0T 8 [<Methods.SP.SP object at 0x000002186DC4C1F0>, <Methods.ASP.ASP object at 0x000002186DC4C2E0>]
]

#follower_tip paths
# pkl_paths=[
# "Storage//follower_tip_force//2566f916-ceb7-4816-9c71-41e5cd8c6fba.pkl",# k 4 N0L 100 N0T 21 [<Methods.Shooting.Shooting object at 0x0000020F90EB2FA0>]
# "Storage//follower_tip_force//a629f969-026c-4c33-a30e-6285bced818a.pkl",# k 2 N0L 100 N0T 21 [<Methods.SP.SP object at 0x000001F408420EB0>, <Methods.ASP.ASP object at 0x000001F40836B820>]
# "Storage//follower_tip_force//166dedf6-762c-45ea-8162-dc90f861822c.pkl",# k 4 N0L 100 N0T 21 [<Methods.SP.SP object at 0x000002882851FE80>, <Methods.ASP.ASP object at 0x000002882845B7F0>]
# "Storage//follower_tip_force//59d42693-1db9-447e-af22-b6dabb5967b0.pkl",# k 8 N0L 100 N0T 21 [<Methods.SP.SP object at 0x0000013E2AF00EB0>, <Methods.ASP.ASP object at 0x0000013E2AE3A820>]
# ]

all_methods = []
for path in pkl_paths:
    with open(path, 'rb') as file:
        sim = pickle.load(file)
        all_methods += sim.methods

width_3D = 10
plotter = Plotter(all_methods[0].N0T,all_methods[0].MP.L,width_3D,all_methods[0]) #compare wrt the first object in the array (should be the shooting method object) 
for mt in all_methods:
    print("mt",mt)
    plotter.plot2D(mt)

plotter.store("plot_2D")
