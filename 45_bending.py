from Misc.MechanicalProperties import * 
from Methods.Shooting import *
from Methods.SP import *
from Methods.ASP import *
from Methods.Simulation import * 
import numpy as np
from Plotter import *
from Misc.Math import *
#-----------------------------------------------------------------------------------------
Rd = 100 # radius of curvature
L = np.pi*Rd/4 
E = 1e7 
usr = np.array([np.pi/(L*4), 0, 0]) 
vsr = np.array([0, 0, 1]) 
G  = E/2; 
A  = 1      
I1 =  1/12 
I2 =  1/12  
I3 = I1+I2  
Kse  = np.diag(np.array([G*A, G*A, E*A]))  
Kbt  = np.diag(np.array([E*I1, E*I2, G*I3]))
invKse = np.linalg.inv(Kse)
invKbt = np.linalg.inv(Kbt)
rhoAg = np.zeros(3)
MP = MechanicalProperties(0,0,0,0,0,0,0,0,0,0,True)
MP.L = L
MP.usr = usr
MP.vsr = vsr
MP.Kse = Kse
MP.Kbt = Kbt 
MP.invKse = invKse
MP.invKbt = invKbt
MP.rhoAg = rhoAg
#-----------------------------------------------------------------------
N0L = 100
max_step = L/N0L
N0T = 21

NL = np.array([3000,0,0])
ML = np.zeros(3)
BCLmax = np.concatenate([NL,ML])
arr_BCL = np.linspace(np.zeros(6),BCLmax,N0T)
IF = True

# k = 8
# k = 4
k = 2

SM  = Shooting(MP,N0T,N0L,max_step)

qk0 = np.array([k]*6)
SP_c = SP(MP,N0T,N0L,max_step,qk0)

qk1 = np.array([k]*3)
ASP_c = ASP(MP,N0T,N0L,max_step,qk1)

methods = [SP_c,ASP_c]
# methods = [SM]

sim = Simulation(methods,arr_BCL,IF)

for i in range(N0T):
    print("------------------------------")
    print("i",i+1,"of",N0T)
    sim.run(i)

store_sim(sim,"45_bending",k)

